var util = require('util');

exports.action = {
  name:                   'login',
  description:            'login',
  blockedConnectionTypes: [],
  outputExample:          {},
  matchExtensionMimeType: false,
  version:                1.0,
  toDocument:             true,
  middleware:             [],

  inputs: {
    username: String,
    password: String
  },

  run: function(api, data, next){
    var error = null;
    var req = util._extend({ body: data.connection.params }, data.connection.rawConnection.req);
    var res = data.connection.rawConnection.res;
    var connection = data.connection;

    api.passport.authenticate("local", {session: true},
      function (err, user, info, extra) {
        if (err) {
          connection.error = err;
          return next(null, false);
        }

        if (!user) {
          connection.rawConnection.responseHttpCode = 401;
          return next(new Error('Unauthorized: ' + info.message));
        }

        // We should store fingerprint to make possible session storage
        user.fingerprint = connection.fingerprint;

        connection.rawConnection.req.logIn(user, function () {
          next();
        });
      }
    )(req, res, next);
  }
};
