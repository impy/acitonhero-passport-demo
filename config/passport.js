exports.default = {
  passport: function(api){
    return {
      strategiesPath: api.projectRoot + '/strategies'
    };
  }
};
