exports.default = {
  session: function(api){
    return {
      prefix: '__sess:',
      duration: 60 * 60 * 4 * 1000 /** 4 Hours */
    };
  }
};

exports.test = {
  session: function(api){
    return {};
  }
};

exports.production = {
  session: function(api){
    return {};
  }
};
