exports.default = {
  mongoose: function(api){
    return {
      enable: true,
      startMongo: true,
      connectionURL: process.env.MONGODB || process.env.MONGOLAB_URI || 'mongodb://localhost/test',
      debug: true,
      modelPath: api.projectRoot + '/models'
    };
  }
};
