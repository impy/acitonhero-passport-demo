module.exports = {
  loadPriority:  900,
  startPriority: 1000,
  stopPriority:  1000,
  initialize: function(api, next){
    api.session = {
      prefix: api.config.session.prefix,
      duration: api.config.session.duration
    };

    api.session.save = function(fingerprint, session, next) {
      var key = api.session.prefix + fingerprint;

      api.cache.save(key, session, api.session.duration, function(error) {
        if (typeof next === "function") {
          return next(error);
        }
      });
    };

    api.session.load = function(fingerprint, next){
      var key = api.session.prefix + fingerprint;

      api.cache.load(key, function(error, session, expireTimestamp, createdAt, readAt){
        if (typeof next === "function") {
          next(error, session, expireTimestamp, createdAt, readAt);
        }
      });
    };

    api.session.delete = function(fingerprint, next){
      var key = api.session.prefix + fingerprint;
      api.cache.destroy(key, function(error, resp) {
        if (typeof next === "function") {
          next(error, resp);
        }
      });
    };

    setupSessionMiddleware = {
      name: 'setup-session',
      global: true,
      preProcessor: function (data, next) {
        data.connection.rawConnection.req.session = { passport: { user: { fingerprint: data.connection.fingerprint }}};
        next();
      }
    };

    api.actions.addMiddleware(setupSessionMiddleware);

    next();
  }
};
