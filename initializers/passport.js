var passport = require('passport');
var path = require('path');
var fs = require('fs');

module.exports = {
  loadPriority:  1000,
  startPriority: 1000,
  stopPriority:  1000,
  initialize: function(api, next){
    api.passport = passport;

    // Create a session record after user logs in
    api.passport.serializeUser(function (user, done) {
      api.session.save(user.fingerprint, user, function(err) {
        return done(err, user.id);
      });
    });

    // Load a user by session id (request fingerprint)
    api.passport.deserializeUser(function (user, done) {
      api.session.load(user.fingerprint, function(err, session) {
        // Don't refresh user until some parts are not ready
        return done(err, session);

        // if (err) {
        //   return done(err);
        // }
        // api.mongoose.models.user.findOne(session.id, done);
      });
    });

    // Load strategies
    var dir = path.normalize(api.config.passport.strategiesPath);
    fs.readdirSync(dir).forEach(function(file) {
      api.passport.use(require(api.config.passport.strategiesPath + '/' + file));
    });

    // Mirror raw request to passport middleware (like for express)
    var passportMiddleware =  {
      name: 'setup-passport',
      global: true,
      preProcessor:function (data, next) {
        api.passport.initialize()(data.connection.rawConnection.req, data.connection.rawConnection.res, function () {
          api.passport.session()(data.connection.rawConnection.req, data.connection.rawConnection.res, function () {
            next();
          });
        });
      }
    };

    // Protect the endpoints here and authenticate them by tokens
    var authMiddleware =  {
      name: 'setup-auth-middleware',
      global: true,
      preProcessor:function (data, next) {
        if (data.connection.rawConnection.req.isAuthenticated()) {
          return next();
        }

        // Required authorization
        if (!!data.actionTemplate.authenticated) {
          // prepare auth
        }

        next();
      }
    };

    // Register passportjs middleware
    api.actions.addMiddleware(passportMiddleware);
    api.actions.addMiddleware(authMiddleware);

    next();
  },
  start: function(api, next){
    next();
  },
  stop: function(api, next){
    next();
  }
};


