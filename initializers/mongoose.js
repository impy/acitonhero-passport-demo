var mongoose = require('mongoose');
var path = require('path');
var fs = require('fs');

module.exports = {
  startPriority: 800,
  initialize: function(api, next) {
    next();
  },
  start: function(api, next){
    api.mongoose = mongoose.connect(api.config.mongoose.connectionURL);

    if(api.mongoose.models === null) {
      api.mongoose.models = {};
    }

    var dir = path.normalize(api.config.mongoose.modelPath);
    fs.readdirSync(dir).forEach(function(file) {
      var nameParts = file.split("/");
      var name = nameParts[(nameParts.length - 1)].split(".")[0];
      if(name.indexOf('-') > -1) {
        name = name.split("-")[1];
      }
      api.mongoose.models[name] = require(api.config.mongoose.modelPath + '/' + file);
    });

    next();
  },
  stop: function(api, next){
    api.mongoose.connection.close();
    next();
  }
};
